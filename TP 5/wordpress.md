# III. Setup de WordPress : base

- [TP5 : Hébergement d'une solution libre et opensource](./README.md)
    - [III. Setup de WordPress : base](#iii-setup-de-wordpress--base)
        - [1. Setup de Apache et PHP](#1-setup-de-apache-php-sur-webtp5linux)
        - [2. Setup de MariaDB](#2-setup-de-mariadb-sur-dbtp5linux)
        - [3. Installation de WordPress](#3-installation-de-wordpress-sur-webtp5linux)

## 1. Setup de apache & php (sur `web.tp5.linux`)

**Installation du service `apache2` :**

```bash
# Installation du package
luka@web:~$ sudo apt install apache2

# Lancement du service
luka@web:~$ sudo systemctl start apache2

# Activation du service
luka@web:~$ sudo systemctl enable apache2
```

**Installation de PHP-FPM :**

```bash
luka@web:~$ sudo apt install gnupg

# Installation du repository
luka@web:~$ wget -q https://packages.sury.org/php/apt.gpg -O- | sudo apt-key add -
luka@web:~$ sudo echo "deb https://packages.sury.org/php/ buster main" | tee /etc/apt/sources.list.d/php.list

# Installation de php
luka@web:~$ sudo apt update
luka@web:~$ sudo apt install php php-fpm

# Installation des extensions php
luka@web:~$ sudo apt install php-curl php-gd php-intl php-mbstring php-soap php-xml php-xmlrpc php-zip php-mysql php7.4-imagick
luka@web:~$ sudo systemctl restart php7.4-fpm.service

# Activation de php pour apache
luka@web:~$ sudo a2enmod proxy_fcgi setenvif
Considering dependency proxy for proxy_fcgi:
Enabling module proxy.
Enabling module proxy_fcgi.
Module setenvif already enabled
To activate the new configuration, you need to run:
  systemctl restart apache2
luka@web:~$ sudo a2enconf php7.4-fpm
Enabling conf php7.4-fpm.
To activate the new configuration, you need to run:
  systemctl reload apache2
luka@web:~$ sudo systemctl reload apache2
```

**Configuration de Apache pour qu'il fonctionne avec PHP-FPM :**

Le fichier de configuration à utiliser est accesible [à ce lien](./sources/000-default.conf)

```bash
# Edition du fichier de configuration
luka@web:~$ sudo nano /etc/apache2/sites-available/000-default.conf
luka@web:~$ sudo systemctl restart apache2.service
```

**Ouverture des ports du serveur web :**

```bash
luka@web:/var/www/html$ sudo firewall-cmd --add-port=80/tcp --permanent
luka@web:/var/www/html$ sudo firewall-cmd --reload
```

---

## 2. Setup de MariaDB (sur `db.tp5.linux`)

**Installation du service MariaDB :**

```bash
luka@db:~$ sudo apt install mariadb-server
```

**Configuration secure de Mysql :**

```bash
luka@db:~$ sudo mysql_secure_installation
Enter current password for root (enter for none): # Enter
Switch to unix_socket authentication [Y/n] # Yes
Change the root password? [Y/n] # Yes
Remove anonymous users? [Y/n] # Yes
Disallow root login remotely? [Y/n] # Yes
Remove test database and access to it? [Y/n] # Yes
Reload privilege tables now? [Y/n] # Yes
```

**Création d'un utilisateur & une DB pour Wordpress :**

- Accès à MariaDB
    ```bash
    luka@db:~$ sudo mariadb
    ```
- Création de la base de données `wordpress`
    ```sql
    MariaDB [(none)]> CREATE DATABASE wordpress DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
    Query OK, 1 row affected (0.000 sec)
    ```
- Création de l'utilisateur et don des accès à la DB
    ```sql
    MariaDB [(none)]> GRANT ALL ON wordpress.* TO 'wordpress_user'@'10.105.1.11' IDENTIFIED BY 'wordpress_passwd';
    Query OK, 0 rows affected (0.005 sec)
    ```
- Rechargement & application des pivilèges
    ```sql
    MariaDB [(none)]> FLUSH PRIVILEGES;
    Query OK, 0 rows affected (0.001 sec)
    ```
- Quitter MariaDB
    ```sql
    MariaDB [(none)]> EXIT
    Bye
    ```

**Ouverture de l'accès à MySQL à distance :**

```bash
# Via le firewall
luka@db:~$ sudo firewall-cmd --add-port=3306/tcp --permanent
luka@db:~$ sudo firewall-cmd --reload

# Via la configuration de Mysql
luka@db:~$ sudo nano /etc/mysql/mariadb.conf.d/50-server.cnf
luka@db:~$ sudo systemctl restart mariadb
```

[Lien vers le fichier de configuration de MariaDB](./sources/50-server.cnf)

---

## 3. Installation de WordPress (sur `web.tp5.linux`)


- Nettoyage
    ```bash
    # Suppression des fichiers dans la web directory
    luka@web:~$ sudo rm /var/www/html/index.html /var/www/html/info.php
    ```
- Téléchargement de WordPress
    ```bash
    # Installation de CURL
    luka@web:~$ sudo apt install curl

    # Téléchargement de WordPress
    luka@web:~$ cd /tmp
    luka@web:/tmp$ curl -LO https://wordpress.org/latest.tar.gz

    # Extraction de l'archive
    luka@web:/tmp$ tar xzvf latest.tar.gz

    # Copiage du sample de la conf
    luka@web:/tmp$ cp /tmp/wordpress/wp-config-sample.php /tmp/wordpress/wp-config.php

    # Copiage de wordpress dans la web directory
    luka@web:/tmp$ sudo cp -a /tmp/wordpress/. /var/www/html

    # Application des permissions
    luka@web:/tmp$ sudo chown -R www-data:www-data /var/www/html
    ```
- Configuration de WordPress ([Lien vers le fichier de conf Wordpress](./sources/wp-config.php))
    ```bash
    luka@web:/var/www/html$ sudo nano wp-config.php
    ```

**Testing time :**

Lancement d'un navigateur sur `10.105.1.11` : WordPress est fonctionnel !

[**Suite : IV. Advanced features**](./advanced-features.md)