# IV. Advanced features

- [TP5 : Hébergement d'une solution libre et opensource](./README.md)
    - [IV. Advanced features](#iv-advanced-features)
        - [1. Setup d'un reverse proxy](#1-setup-dun-reverse-proxy-sur-proxytp5linux)
        - [2. Mise en place du HTTPS](#2-mise-en-place-du-https-sur-proxytp5linux)
        - [3. Setup de Fail2Ban](#3-setup-de-fail2ban-sur-toutes-les-machines)
        - [4. Setup d'un monitoring Netdata](#4-setup-dun-monitoring-netdata-sur-toutes-les-machines)

## 1. Setup d'un reverse proxy (sur `proxy.tp5.linux`)

**Installation de NGINX :**

```bash
luka@proxy:~$ sudo apt install nginx
```

**Configuration de NGINX :**

```bash
luka@proxy:~$ sudo nano /etc/nginx/conf.d/proxy.conf
luka@proxy:~$ sudo systemctl restart nginx
```

Configuration de NGINX disponbile [à ce lien](./sources/proxy-v1.conf)

**Sur `db.tp5.linux` : Changements dans la BDD de WordPress :**

Comme WordPress stocke le l'adresse du site web en base de données pour effectuer ses redirections/routes/..., nous allons le redéfinir.

- On sélectionne la Base de données `wordpress`
    ```sql
    MariaDB [(none)]> USE wordpress;
    ```
- On exécute le changement le lien dans les options
    ```sql
    MariaDB [wordpress]> UPDATE wp_options SET option_value = replace(option_value, '10.105.1.11', 'my.meow.wordpress') WHERE option_name = 'home' OR option_name = 'siteurl';
    ```
- On exécute le changement de lien dans les posts
    ```sql
    MariaDB [wordpress]> UPDATE wp_posts SET guid = replace(guid, '10.105.1.11','my.meow.wordpress');

    MariaDB [wordpress]> UPDATE wp_posts SET post_content = replace(post_content, '10.105.1.11', 'my.meow.wordpress');

    MariaDB [wordpress]> UPDATE wp_postmeta SET meta_value = replace(meta_value,'10.105.1.11','my.meow.wordpress');
    ```

**Sur `web.tp5.linux` : Fermeture du prot 80 (sauf pour le proxy) :**

```bash
# Donner l'accès au port 80 aux adresses "internes"
luka@web:/var/www/html$ sudo firewall-cmd --zone=internal --add-port=80/tcp --permanent

# Ajout de l'adresse du proxy aux accès "internes"
luka@web:/var/www/html$ sudo firewall-cmd --zone=internal --add-source=10.105.1.13 --permanent

# On enlève l'accès publique au port 80
luka@web:/var/www/html$ sudo firewall-cmd --zone=public --remove-port=80/tcp --permanent

# Reload du firewall pour appliquer les changements
luka@web:/var/www/html$ sudo firewall-cmd --reload
```

**Penser à modifier le fichier hosts du PC !**

- Pour Windows, dans le fichier : `C:\Windows\System32\drivers\etc\hosts`
- Pour GNU/Linux, dans le fichier : `/etc/hosts`

Insérer une nouvelle ligne : `10.105.1.13    my.meow.wordpress`

**Testing time :**

Lancement d'un navigateur sur `10.105.1.11` : L'accès y est impossible.

Lancement d'un navigateur sur `my.meow.wordpress` : Tout est fonctionnel !

## 2. Mise en place du HTTPS (sur `proxy.tp5.linux`)

Nous allons utilsier `certbot` pour activer notre certificat SSL.

- Installation de `snapd`
    ```bash
    # Installation de snapd
    luka@proxy:~$ sudo apt install snapd

    # Vérification que snapd est à jour
    luka@proxy:~$ sudo snap install core; sudo snap refresh core
    ```
- Retrait de toute trace de certbot qui pourrait être sur la machine (au cas où)
    ```bash
    luka@proxy:~$ sudo apt-get remove certbot
    ```
- Installation de certbot
    ```bash
    # Installation de certbot
    luka@proxy:~$ sudo snap install --classic certbot
    
    # Préparation de la commande certbot
    luka@proxy:~$ sudo ln -s /snap/bin/certbot /usr/bin/certbot
    ```
- Installation du certificat
    ```bash
    luka@proxy:~$ sudo mkdir /etc/ssl/private
    luka@proxy:~$ sudo openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/ssl/private/nginx-selfsigned.key -out /etc/ssl/certs/nginx-selfsigned.crt
    ```
- Configuration de NGINX
    ```bash
    luka@proxy:~$ sudo nano /etc/nginx/conf.d/proxy.conf
    luka@proxy:~$ sudo systemctl restart nginx
    ```

Fichier de conf disponible [à ce lien](./sources/proxy-v2.conf)

## 3. Setup de Fail2Ban (sur toutes les machines)

- Installation de Fail2Ban
    ```bash
    luka@proxy:~$ sudo apt install fail2ban
    ```
- Configuration de Fail2Ban
    ```bash
    luka@proxy:/etc/fail2ban$ sudo cp jail.conf jail.local
    luka@proxy:/etc/fail2ban$ sudo nano jail.local
    ```

Fichier de conf disponible [à ce lien](./sources/jail.local)

## 4. Setup d'un monitoring Netdata (sur toutes les machines)

- Installation de Netdata
    ```bash
    # Install de wget
    luka@proxy:~$ sudo apt install wget

    # Téléchargement de netdata
    luka@proxy:~$ wget -O /tmp/netdata-kickstart.sh https://my-netdata.io/kickstart.sh && sh /tmp/netdata-kickstart.sh

    # Lancement de netdata
    luka@proxy:~$ sudo systemctl start netdata
    luka@proxy:~$ sudo systemctl enable netdata
    Synchronizing state of netdata.service with SysV service script with /lib/systemd/systemd-sysv-install.
    Executing: /lib/systemd/systemd-sysv-install enable netdata

    # Ouverture du port
    luka@proxy:~$ sudo firewall-cmd --permanent --add-port=19999/tcp
    luka@proxy:~$ sudo firewall-cmd --reload
    ```