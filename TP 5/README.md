# TP5 : Hébergement d'une solution libre et opensource

- [TP5 : Hébergement d'une solution libre et opensource](#tp5--hébergement-dune-solution-libre-et-opensource)
    - [I. Présentation du projet](#i-présentation-du-projet)
    - [III. Setup de WordPress : base](./wordpress.md)
        - [1. Setup de Apache et PHP](./wordpress.md#1-setup-de-apache-php-sur-webtp5linux)
        - [2. Setup de MariaDB](./wordpress.md#2-setup-de-mariadb-sur-dbtp5linux)
        - [3. Installation de WordPress](./wordpress.md#3-installation-de-wordpress-sur-webtp5linux)
    - [IV. Advanced features](./advanced-features.md)
        - [1. Setup d'un reverse proxy](./advanced-features.md#1-setup-dun-reverse-proxy-sur-proxytp5linux)
        - [2. Mise en place du HTTPS](./advanced-features.md#2-mise-en-place-du-https-sur-proxytp5linux)
        - [3. Setup de Fail2Ban](./advanced-features.md#3-setup-de-fail2ban-sur-toutes-les-machines)

## I. Présentation du sujet

Pour ce TP, j'ai choisi d'installer un WordPress, qui s'étendra sur plusieurs serveurs. WordPress nécessite d'avoir un serveur web avec PHP (nous utiliserons PHP-FPM) et un serveur MariaDB. Nous ajouterons divers autres serveurs autour : Reverse proxy, sauvegardes, ...

**Liste des machine utilisées :**

| Machine           | IP            | Service               |
|-------------------|---------------|-----------------------|
| `web.tp5.linux`   | `10.105.1.11` | Serveur Web           |
| `db.tp5.linux`    | `10.105.1.12` | Server database       | 
| `proxy.tp5.linux` | `10.105.1.13` | Serveur reverse-proxy |
| `save.tp5.linux`  | `10.105.1.14` | Serveur de sauvegarde |

## II. Prérequis

L'OS utilisé sur les machines de ce TP est un `Debian 11 (Bullseye)`.

Prérequis pour chacune des machines :

- Installation des packages "de base" (`sudo`, `nano`, `network-manager`, `firewalld` ...)
- Une adresse IP locale, statique ou dynamique
- Le hostname défini
- Le firewall actif, qui ne laisse passer que le strict nécessaire
- Le SSH fonctionnel avec un échange de clé
- Un accès Internet (une route par défaut, une carte NAT c'est très bien)
- De la résolution de nom

## III. Setup de WordPress : base

[Lien vers le README dédié à cette partie](./wordpress.md)

## IV. Advanced features

[Lien vers le README dédié à cette partie](./advanced-features.md)