# TP2 : Gestion de service

- [TP2 : Gestion de service](#tp2--gestion-de-service)
- [I. Un premier serveur web](#i-un-premier-serveur-web)
  - [1. Installation](#1-installation)
  - [2. Avancer vers la maîtrise du service](#2-avancer-vers-la-maîtrise-du-service)
- [II. Une stack web plus avancée](#ii-une-stack-web-plus-avancée)
  - [2. Setup](#2-setup)
    - [A. Base de données](#a-base-de-données)
    - [B. Serveur Web et NextCloud](#b-serveur-web-et-nextcloud)
    - [C. Finaliser l'installation de NextCloud](#c-finaliser-linstallation-de-nextcloud)

## I. Un premier serveur web

### 1. Installation

🌞 **Installer le serveur Apache**

```bash
[luka@web ~]$ sudo dnf install httpd
Last metadata expiration check: 0:19:30 ago on Tue 15 Nov 2022 09:52:17 AM CET.
Dependencies resolved.
[...]
```

🌞 **Démarrer le service Apache**

```bash
# Lancement du service apache
[luka@web ~]$ sudo systemctl start httpd

# Activation du lancement automatique au démarrage
[luka@web ~]$ sudo systemctl enable httpd
Created symlink /etc/systemd/system/multi-user.target.wants/httpd.service → /usr/lib/systemd/system/httpd.service.

# Récupération du port sur lequel écoute le service
[luka@web ~]$ ss -laptn
State   Recv-Q  Send-Q   Local Address:Port    Peer Address:Port   Process
[...]
LISTEN  0       511                  *:80                 *:*

# Ouverture du port 80 sur le firewall
[luka@web ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
success
[luka@web ~]$ sudo firewall-cmd --reload
success
```

🌞 **TEST**

- Vérification que le service est démarré
    ```bash
    # Avec la commande is-active
    [luka@web ~]$ systemctl is-active httpd
    active

    # Avec la commande status
    [luka@web ~]$ systemctl status httpd
    ● httpd.service - The Apache HTTP Server
        Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; vendor>
        Active: active (running) since Tue 2022-11-15 10:25:45 CET; 7min ago
        [...]
    ```
- Vérification que le service est activé pour se lancer au démarrage
    ```bash
    [luka@web ~]$ systemctl is-enabled httpd
    enabled
    ```
- Vérification que l'on peut joindre le serveur web localement
    ```bash
    [luka@web ~]$ curl localhost
    <!doctype html>
    <html>
    [...]
    </html>
    ```
- Vérification que l'on peut joindre le serveur web depuis notre PC
    ```
    PS C:\Users\lukab> curl 10.102.1.11
    curl : HTTP Server Test Page
    This page is used to test the proper operation of an HTTP server after it
    [...]
    ```

### 2. Avancer vers la maîtrise du service

🌞 **Le service Apache...**

```bash
[luka@web ~]$ cat /etc/systemd/system/multi-user.target.wants/httpd.service
    [Unit]
    Description=The Apache HTTP Server
    Wants=httpd-init.service
    After=network.target remote-fs.target nss-lookup.target httpd-init.service
    Documentation=man:httpd.service(8)

    [Service]
    Type=notify
    Environment=LANG=C

    ExecStart=/usr/sbin/httpd $OPTIONS -DFOREGROUND
    ExecReload=/usr/sbin/httpd $OPTIONS -k graceful
    KillSignal=SIGWINCH
    KillMode=mixed
    PrivateTmp=true
    OOMPolicy=continue

    [Install]
    WantedBy=multi-user.target
```

🌞 **Déterminer sous quel utilisateur tourne le processus Apache**

Apache tourne sous l'utilisateur `apache`.

- Dans la configuration de apache
    ```bash
    [luka@web ~]$ cat /etc/httpd/conf/httpd.conf | grep "User"
    User apache
    [...]
    ```
- Avec la liste des processus en cours d'exécution
    ```bash
    [luka@web ~]$ ps -ef | grep "apache"
    apache     33318   33317  0 10:25 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
    apache     33319   33317  0 10:25 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
    apache     33320   33317  0 10:25 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
    apache     33321   33317  0 10:25 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
    ```
- Vérification des droits sur la home d'Apache
    ```bash
    [luka@web ~]$ ls -al /usr/share/testpage
    total 12
    drwxr-xr-x.  2 root root   24 Nov 15 10:11 .
    drwxr-xr-x. 82 root root 4096 Nov 15 10:11 ..
    -rw-r--r--.  1 root root 7620 Jul  6 04:37 index.html

    # Le fichier appartient à root, mais le troisième "r" signifie que le fichier est accessible en lecture pour tous.
    ```

🌞 **Changer l'utilisateur utilisé par Apache**

- Création d'un utilisateur
    ```bash
    # Récupération des infos sur l'utilisateur apache
    [luka@web ~]$ cat /etc/passwd | grep "apache"
    apache:x:48:48:Apache:/usr/share/httpd:/sbin/nologin

    # Création de l'utilisateur avec ces spécificités
    [luka@web ~]$ sudo useradd web -d /usr/share/httpd -s /sbin/nologin

    # Vérification 
    [luka@web ~]$ cat /etc/passwd | grep "web"
    web:x:1001:1001::/usr/share/httpd:/sbin/nologin
    ```
- Modification de la configuration d'apache
    ```bash
    # Changement de l'utilisateur dans la configuration d'apache
    [luka@web ~]$ sudo nano /etc/httpd/conf/httpd.conf
    [luka@web ~]$ cat /etc/httpd/conf/httpd.conf | grep "User"
    User web

    # Redémarrage d'apache
    [luka@web ~]$ sudo systemctl restart httpd
    ```
- Vérification que les changements sont pris en compte
    ```bash
    [luka@web ~]$ ps -ef | grep "web"
    web        33667   33666  0 11:14 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
    web        33668   33666  0 11:14 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
    web        33669   33666  0 11:14 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
    web        33670   33666  0 11:14 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
    ```

🌞 **Faites en sorte que Apache tourne sur un autre port**

- Modification de la configuration d'apache
    ```bash
    # Modification de la configuration
    [luka@web ~]$ sudo nano /etc/httpd/conf/httpd.conf
    [luka@web ~]$ cat /etc/httpd/conf/httpd.conf | grep "Listen"
    Listen 8080

    # Redémarrage d'apache
    [luka@web ~]$ sudo systemctl restart httpd
    ```
- Gestion du firewall
    ```bash
    # Fermeture de l'ancien port utilisé (port 80)
    [luka@web ~]$ sudo firewall-cmd --remove-port=80/tcp --permanent
    success

    # Ouverture du nouveau port (port 8080)
    [luka@web ~]$ sudo firewall-cmd --add-port=8080/tcp --permanent
    success
    
    # Rechargement du firewall pour appliquer les changements
    [luka@web ~]$ sudo firewall-cmd --reload
    success
    ```
- Vérification que le nouveau port est pris en compte
    - Avec la commande `ss`
        ```bash
        [luka@web ~]$ ss -laptn
        State   Recv-Q  Send-Q   Local Address:Port    Peer Address:Port   Process
        [...]
        LISTEN  0       511                  *:8080               *:*
        ```
    - Avec un `curl` depuis mon PC
        ```
        PS C:\Users\lukab> curl 10.102.1.11:8080
        curl : HTTP Server Test Page
        This page is used to test the proper operation of an HTTP server after it
        [...]
        ```

📁 **Fichier `/etc/httpd/conf/httpd.conf`**

[Le fichier est accesible à ce lien](./sources/httpd.conf)

## II. Une stack web plus avancée

### 2. Setup

### A. Base de données

🌞 **Install de MariaDB sur `db.tp2.linux`**

```bash
# Installation de mariadb
[luka@db ~]$ sudo dnf install mariadb-server
[...]
Complete!

# Lancement et activation de mariadb au démarrage
[luka@db ~]$ sudo systemctl start mariadb
[luka@db ~]$ sudo systemctl enable mariadb
Created symlink /etc/systemd/system/mysql.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/mysqld.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/multi-user.target.wants/mariadb.service → /usr/lib/systemd/system/mariadb.service

# Configuration sécurisée de mariadb
[luka@db ~]$ sudo mysql_secure_installation
[...]
Thanks for using MariaDB!

# Ouverture du port utilisé par mariadb (port 3306)
[luka@db ~]$ sudo firewall-cmd --add-port=3306/tcp --permanent
success
[luka@db ~]$ sudo firewall-cmd --reload
success
```

🌞 **Préparation de la base pour NextCloud**

```bash
[luka@db ~]$ sudo mysql -u root -p

MariaDB [(none)]> CREATE USER 'nextcloud'@'10.102.1.11' IDENTIFIED BY 'nextc
loud';
Query OK, 0 rows affected (0.006 sec)

MariaDB [(none)]> CREATE DATABASE IF NOT EXISTS nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
Query OK, 1 row affected (0.000 sec)

MariaDB [(none)]> GRANT ALL PRIVILEGES ON nextcloud.* TO 'nextcloud'@'10.102.1.11';
Query OK, 0 rows affected (0.003 sec)

MariaDB [(none)]> FLUSH PRIVILEGES;
Query OK, 0 rows affected (0.000 sec)
```

🌞 **Exploration de la base de données**

```bash
# Installation de mysql
[luka@web ~]$ sudo dnf install -y mysql
[...]
Complete!

# Connexion à la database
[luka@web ~]$ mysql -u nextcloud -h 10.102.1.12 -p

# Exploration de la database
mysql> SHOW DATABASES;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| nextcloud          |
+--------------------+
2 rows in set (0.00 sec)

mysql> USE nextcloud;
Database changed
mysql> SHOW TABLES;
Empty set (0.00 sec)
```

🌞 **Trouver une commande SQL qui permet de lister tous les utilisateurs de la base de données**

```bash
[luka@db ~]$ sudo mysql -u root -p

MariaDB [(none)]> SELECT user FROM mysql.user;
+-------------+
| User        |
+-------------+
| nextcloud   |
| mariadb.sys |
| mysql       |
| root        |
+-------------+
```

### B. Serveur Web et NextCloud

🌞 **Install de PHP**

```bash
# On ajoute le dépôt CRB
[luka@web ~]$ sudo dnf config-manager --set-enabled crb

# On ajoute le dépôt REMI
[luka@web ~]$ sudo dnf install dnf-utils http://rpms.remirepo.net/enterprise/remi-release-9.rpm -y
[...]
Complete!

# On liste les versions de PHP dispos, au passage on va pouvoir accepter les clés du dépôt REMI
[luka@web ~]$ dnf module list php
[...]
Hint: [d]efault, [e]nabled, [x]disabled, [i]nstalled

# On active le dépôt REMI pour récupérer une version spécifique de PHP, celle recommandée par la doc de NextCloud
[luka@web ~]$ sudo dnf module enable php:remi-8.1 -y
Complete!

# Eeeet enfin, on installe la bonne version de PHP : 8.1
[luka@web ~]$ sudo dnf install -y php81-php
[...]
Complete!
```

🌞 **Install de tous les modules PHP nécessaires pour NextCloud**

```bash
[luka@web ~]$ sudo dnf install -y libxml2 openssl php81-php php81-php-ctype php81-php-curl php81-php-gd php81-php-iconv php81-php-json php81-php-libxml php81-php-mbstring php81-php-openssl php81-php-posix php81-php-session php81-php-xml php81-php-zip php81-php-zlib php81-php-pdo php81-php-mysqlnd php81-php-intl php81-php-bcmath php81-php-gm
[...]
Complete!
```

🌞 **Récupérer NextCloud**

- Créer le dossier `/var/www/tp2_nextcloud/`
    ```bash
    [luka@web ~]$ sudo mkdir /var/www/tp2_nextcloud
    [luka@web ~]$ ls /var/www
    cgi-bin  html  tp2_nextcloud
    ```
- Download Nextcloud
    ```bash
    # Download
    [luka@web ~]$ curl https://download.nextcloud.com/server/prereleases/nextcloud-25.0.0rc3.zip
    [luka@web ~]$ ls
    nextcloud.zip

    # Extraire
    [luka@web ~]$ sudo dnf install unzip
    Complete!
    [luka@web ~]$ unzip nextcloud.zip
    [...]
    [luka@web ~]$ ls
    nextcloud  nextcloud.zip

    # Déplacement du contenu du dossier nextcloud vers notre racine web
    [luka@web nextcloud]$ sudo mv * /var/www/tp2_nextcloud/
    [luka@web nextcloud]$ sudo mv .* /var/www/tp2_nextcloud/

    # Application des permissions
    [luka@web ~]$ sudo chown -R apache:apache /var/www/tp2_nextcloud/
    ```

🌞 **Adapter la configuration d'Apache**

```bash
[luka@web ~]$ sudo nano /etc/httpd/conf.d/nextcloud.conf
[luka@web ~]$ cat /etc/httpd/conf.d/nextcloud.conf
<VirtualHost *:80>
  DocumentRoot /var/www/tp2_nextcloud/
  ServerName  web.tp2.linux
  <Directory /var/www/tp2_nextcloud/>
    Require all granted
    AllowOverride All
    Options FollowSymLinks MultiViews
    <IfModule mod_dav.c>
      Dav off
    </IfModule>
  </Directory>
</VirtualHost>
```

🌞 **Redémarrer le service Apache**

```bash
[luka@web ~]$ sudo systemctl restart httpd
```

### C. Finaliser l'installation de NextCloud

🌞 **Exploration de la base de données**

```bash
[luka@db ~]$ sudo mysql -u root -p

MariaDB [(none)]> USE nextcloud;

MariaDB [nextcloud]> SHOW TABLES
    -> ;
+-----------------------------+
| Tables_in_nextcloud         |
+-----------------------------+
| oc_accounts                 |
[...]
+-----------------------------+
95 rows in set (0.001 sec)

# 95 tables ont été crées
```