# TP3 : Amélioration de la solution NextCloud

## Liste des modules

- [Module 1 - Reverse proxy](./Module%201%20-%20Reverse%20Proxy.md)
- [Module 2 - Réplication de base de données]()
- [Module 3 - Sauvegarde de base de données]()
- [Module 4 - Sauvegarde du système de fichiers]()
- [Module 5 - Monitoring](./Module%205%20-%20Monitoring.md)
- [Module 6 - Automatiser le déploiement]()
- [Module 7 - Fail2ban](./Module%207%20-%20Fail2Ban.md)

## Liste des machines utilisées

| Machine           | IP            | Service         |
|-------------------|---------------|-----------------|
|  `web.tp2.linux`  | `10.102.1.11` | Serveur Web     |
|  `db.tp2.linux`   | `10.102.1.12` | Server database |
| `proxy.tp3.linux` | `10.102.1.13` | Serveur proxy   |