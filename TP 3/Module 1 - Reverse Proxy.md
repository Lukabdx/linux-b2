# Module 1 - Reverse proxy

- [Module 1 : Reverse Proxy](#module-1--reverse-proxy)
- [II. Setup](#ii-setup)
    - [1. Install de NGINX](#1-install-de-nginx)
    - [2. Configuration de NGINX](#2-configuration-de-nginx)
    - [3. Modification du fichier hosts](#2-modification-du-fichier-hosts)
    - [4. Désactiver le port 80 sauf pour le proxy](#4-désactiver-le-port-80-sauf-pour-le-proxy)
- [III. HTTPS](#iii-https)

## II. Setup

### 1. Install de NGINX

- Installer le paquet `nginx`
    ```bash
    [luka@proxy ~]$ sudo dnf install nginx
    Complete!
    ```
- Démarrer le service `nginx`
    ```bash
    # Démarrage du service
    [luka@proxy ~]$ sudo systemctl start nginx

    # Activation automatique au lancement
    [luka@proxy ~]$ sudo systemctl enable nginx
    Created symlink /etc/systemd/system/multi-user.target.wants/nginx.service → /usr/lib/systemd/system/nginx.service.
    ```
- Utiliser la commande `ss` pour repérer le port sur lequel NGINX écoute
    ```bash
    [luka@proxy ~]$ ss -laptn | grep "80"
    State         Recv-Q        Send-Q               Local Address:Port               Peer Address:Port       Process
    LISTEN        0             511                        0.0.0.0:80                      0.0.0.0:*
    LISTEN        0             511                           [::]:80                         [::]:*
    ```
- Ouvrir un port dans le firewall pour autoriser le trafic vers NGINX
    ```bash
    [luka@proxy ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
    success
    [luka@proxy ~]$ sudo firewall-cmd --reload
    success
    ```
- Utiliser une commande `ps -ef` pour déterminer sous quel utilisateur tourne NGINX
    ```bash
    [luka@proxy ~]$ ps -ef | grep "nginx"
    root       38865       1  0 11:45 ?        00:00:00 nginx: master process /usr/sbin/nginx
    nginx      38866   38865  0 11:45 ?        00:00:00 nginx: worker process
    nginx      38867   38865  0 11:45 ?        00:00:00 nginx: worker process
    ```
- Vérifier que le page d'accueil NGINX est disponible en faisant une requête HTTP sur le port 80 de la machine
    ```
    PS C:\Users\lukab\.ssh> curl 10.102.1.13
        StatusCode        : 200
        StatusDescription : OK
        Content           : <!doctype html>
        [...]
    ```

### 2. Configuration de NGINX

- Création d'un fichier de configuration NGINX
    ```bash
    [luka@proxy ~]$ sudo nano /etc/nginx/conf.d/proxy.conf
    [luka@proxy ~]$ cat /etc/nginx/conf.d/proxy.conf
    server {
        [...]
    }

    [luka@proxy ~]$ sudo systemctl restart nginx
    ```
- Modification de la configuration de Nextcloud
    ```bash
    [luka@web ~]$ sudo nano /var/www/tp2_nextcloud/config/config.php

    [luka@web ~]$ sudo cat /var/www/tp2_nextcloud/config/config.php | grep "proxy"
        'proxy' => '10.102.1.13',
    ```

### 3. Modification du fichier hosts

```
PS C:\Users\lukab\.ssh> ping proxy.tp3.linux
La requête Ping n’a pas pu trouver l’hôte proxy.tp3.linux. Vérifiez le nom et essayez à nouveau.
PS C:\Users\lukab\.ssh> ping web.tp2.linux

Envoi d’une requête 'ping' sur web.tp2.linux [10.102.1.13] avec 32 octets de données :
Réponse de 10.102.1.13 : octets=32 temps<1ms TTL=64
Réponse de 10.102.1.13 : octets=32 temps<1ms TTL=64
Réponse de 10.102.1.13 : octets=32 temps<1ms TTL=64
Réponse de 10.102.1.13 : octets=32 temps<1ms TTL=64

Statistiques Ping pour 10.102.1.13:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 0ms, Maximum = 0ms, Moyenne = 0ms
```

### 4. Désactiver le port 80 sauf pour le proxy

```bash
# Donner l'accès au port 80 aux adresses "internes"
[luka@web ~]$ sudo firewall-cmd --zone=internal --add-port=80/tcp --permanent
success

# Ajout de l'adresse du proxy aux accès "internes"
[luka@web ~]$ sudo firewall-cmd --zone=internal --add-source=10.102.1.13 --permanent
success

# On enlève l'accès publique au port 80
[luka@web ~]$ sudo firewall-cmd --zone=public --remove-port=80/tcp --permanent
success

# Reload du firewall pour appliquer les changements
[luka@web ~]$ sudo firewall-cmd --reload
success
```

**Test d'accès à Nextcloud directement depuis le serveur web :**

```
PS C:\Users\lukab\.ssh> curl 10.102.1.11
curl : Impossible de se connecter au serveur distant
Au caractère Ligne:1 : 1
+ curl 10.102.1.11
```

**Test d'accès à Nextcloud via le proxy :**

```
PS C:\Users\lukab\.ssh> curl web.tp2.linux
StatusCode        : 200
StatusDescription : OK
Content           : <!DOCTYPE html>
[...]
```

## III. HTTPS

```
IN PROGRESS
```