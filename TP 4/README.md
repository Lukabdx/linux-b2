# TP4 : Conteneur

- [TP4 : Conteneurs](#tp4--conteneurs)
- [I. Docker](#i-docker)
  - [1. Install](#1-install)
  - [3. Lancement de conteneurs](#3-lancement-de-conteneurs)
- [II. Images](#ii-images)
  - [2. Construisez votre propre Dockerfile](2-construisez-votre-propre-dockerfile)
- [III. `docker-compose`](#iii-docker-compose)
  - [2. Make your own meow](#2-make-your-own-meow)

## I. Docker

### 1. Install

🌞 **Installer Docker sur la machine**

- Installation de Docker
    ```bash
    # Désinstallation des anciennes versions
    [luka@docker1 ~]$ sudo yum remove docker \
                    docker-client \
                    docker-client-latest \
                    docker-common \
                    docker-latest \
                    docker-latest-logrotate \
                    docker-logrotate \
                    docker-engine
    No match for argument: docker
    No match for argument: docker-client
    No match for argument: docker-client-latest
    No match for argument: docker-common
    No match for argument: docker-latest
    No match for argument: docker-latest-logrotate
    No match for argument: docker-logrotate
    No match for argument: docker-engine
    No packages marked for removal.
    Dependencies resolved.
    Nothing to do.
    Complete!

    # Installation du repo docker
    [luka@docker1 ~]$ sudo yum install -y yum-utils
    Complete!
    [luka@docker1 ~]$ sudo yum-config-manager \
    --add-repo \
    https://download.docker.com/linux/centos/docker-ce.repo

    # Installation de Docker Engine
    [luka@docker1 ~]$ sudo yum install docker-ce docker-ce-cli containerd.io docker-compose-plugin
    Complete!
    ```
- Démarrage du service
    ```bash
    # Démarrage du service
    [luka@docker1 ~]$ sudo systemctl start docker

    # Activation du démarrage au lancement de la VM
    [luka@docker1 ~]$ sudo systemctl enable docker
    Created symlink /etc/systemd/system/multi-user.target.wants/docker.service → /usr/lib/systemd/system/docker.service.
    ```
- Ajout de mon user au groupe `docker`
    ```bash
    # Ajout du user 'luka' au groupe 'docker'
    [luka@docker1 ~]$ sudo usermod -a -G docker luka

    # Vérification des groupes de 'luka' (après un déco/reco)
    [luka@docker1 ~]$ groups
    luka wheel docker
    ```

### 3. Lancement de conteneurs

🌞 **Utiliser la commande `docker run`**

```bash
# Lancement du docker avec les options demandées
[luka@docker1 ~]$ docker run --name web -d -p 80:8888 -v ~/site-content:/usr/share/nginx/html -v ~/site-conf/meow.conf:/etc/nginx/conf.d/meow.conf -m="1g" --cpus="1.0" nginx
52e4fb5cb937ab6c657f08b2360969fcf745d9dd427942a57bc858942eee1e61

# Vérification que la page web fonctionne
PS C:\Users\lukab> curl 10.104.1.11
StatusCode        : 200
StatusDescription : OK
Content           : <!doctype html>
                    <html lang="en">
                    <head>
                      <meta charset="utf-8">
                      <title>My Meow Page</title>
                    </head>
                    <body>
                      <h2>Meow</h2>
                    </body>
                    </html>
[...]
```

## II. Images

### 2. Construisez votre propre Dockerfile

🌞 **Construire votre propre image**

```bash
# Création du Dockerfile
[luka@docker1 workdir-II]$ nano Dockerfile

# Création des fichiers nécessaires
[luka@docker1 workdir-II]$ cp ~/site-content/index.html index.html
[luka@docker1 workdir-II]$ nano custom.conf

# On build notre image
[luka@docker1 workdir-II]$ docker build . -t papache
[luka@docker1 workdir-II]$ docker images | grep "papache"
papache      latest    3dbac50d57b0   2 minutes ago   225MB

# On peut lancer notre app désormais
[luka@docker1 workdir-II]$ docker run -p 8888:80 papache
AH00558: apache2: Could not reliably determine the server's fully qualified domain name, using 172.17.0.2. Set the 'ServerName' directive globally to suppress this message
```

📁 [Lien vers le Dockerfile](./sources/Dockerfile)

## III. docker-compose

### 2. Make your own meow

🌞 **Conteneurisez votre application**

📁 Lien vers app/Dockerfile

📁 Lien vers app/docker-compose.yml