# TP1 : (re)Familiarisation avec un système GNU/Linux

- [TP1 : (re)Familiarisation avec un système GNU/Linux](#tp1--refamiliarisation-avec-un-système-gnulinux)
  - [0. Préparation de la machine](#0-préparation-de-la-machine)
  - [I. Utilisateurs](#i-utilisateurs)
    - [1. Création et configuration](#1-création-et-configuration)
    - [2. SSH](#2-ssh)
  - [II. Partitionnement](#ii-partitionnement)
    - [2. Partitionnement](#2-partitionnement)
  - [III. Gestion de services](#iii-gestion-de-services)
  - [1. Interaction avec un service existant](#1-interaction-avec-un-service-existant)
  - [2. Création de service](#2-création-de-service)
    - [A. Unité simpliste](#a-unité-simpliste)
    - [B. Modification de l'unité](#b-modification-de-lunité)

## 0. Préparation de la machine

🌞 **Setup de deux machines Rocky Linux configurées de façon basique.**

- Les machines doivent avoir un nom
  - Sur la machine `node1.tp1.b2` :
    ```bash
    [luka@localhost ~]$ sudo hostname node1.tp1.b2
    [luka@localhost ~]$ sudo echo "node1.tp1.b2" | sudo tee /etc/hostname
    node1.tp1.b2
    
    # On redémarre la machine pour appliquer les changements

    [luka@node1 ~]$
    ```
  - Sur la machine `node2.tp1.b2` :
    ```bash
    [luka@localhost ~]$ sudo hostname node2.tp1.b2
    [luka@localhost ~]$ sudo echo "node2.tp1.b2" | sudo tee /etc/hostname
    node2.tp1.b2
    
    # On redémarre la machine pour appliquer les changements

    [luka@node2 ~]$
    ```
- Un accès internet (via la carte NAT)
  - Sur la machine `node1.tp1.b2` :
    ```bash
    [luka@node1 ~]$ ping -c 4 8.8.8.8
    PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
    64 bytes from 8.8.8.8: icmp_seq=1 ttl=113 time=25.5 ms
    64 bytes from 8.8.8.8: icmp_seq=2 ttl=113 time=22.0 ms
    64 bytes from 8.8.8.8: icmp_seq=3 ttl=113 time=22.5 ms
    64 bytes from 8.8.8.8: icmp_seq=4 ttl=113 time=26.2 ms

    --- 8.8.8.8 ping statistics ---
    4 packets transmitted, 4 received, 0% packet loss, time 3203ms
    rtt min/avg/max/mdev = 22.040/24.078/26.248/1.831 ms
    ```
  - Sur la machine `node2.tp1.b2` :
    ```bash
    [luka@node2 ~]$ ping -c 4 8.8.8.8
    PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
    64 bytes from 8.8.8.8: icmp_seq=1 ttl=113 time=23.3 ms
    64 bytes from 8.8.8.8: icmp_seq=2 ttl=113 time=24.0 ms
    64 bytes from 8.8.8.8: icmp_seq=3 ttl=113 time=27.1 ms
    64 bytes from 8.8.8.8: icmp_seq=4 ttl=113 time=24.5 ms

    --- 8.8.8.8 ping statistics ---
    4 packets transmitted, 4 received, 0% packet loss, time 3004ms
    rtt min/avg/max/mdev = 23.260/24.712/27.074/1.431 ms
    ```
- Un accès à un réseau local (les deux machines peuvent se ping) (via la carte Host-Only)
  - Sur la machine `node1.tp1.b2` :
    ```bash
    [luka@node1 ~]$ ping -c 4 10.101.1.12
    PING 10.101.1.12 (10.101.1.12) 56(84) bytes of data.
    64 bytes from 10.101.1.12: icmp_seq=1 ttl=64 time=0.611 ms
    64 bytes from 10.101.1.12: icmp_seq=2 ttl=64 time=0.222 ms
    64 bytes from 10.101.1.12: icmp_seq=3 ttl=64 time=1.00 ms
    64 bytes from 10.101.1.12: icmp_seq=4 ttl=64 time=0.194 ms

    --- 10.101.1.12 ping statistics ---
    4 packets transmitted, 4 received, 0% packet loss, time 3083ms
    rtt min/avg/max/mdev = 0.194/0.507/1.001/0.329 ms
    ```
  - Sur la machine `node2.tp1.b2` :
    ```bash
    [luka@node2 ~]$ ping -c 4 10.101.1.11
    PING 10.101.1.11 (10.101.1.11) 56(84) bytes of data.
    64 bytes from 10.101.1.11: icmp_seq=1 ttl=64 time=0.309 ms
    64 bytes from 10.101.1.11: icmp_seq=2 ttl=64 time=1.00 ms
    64 bytes from 10.101.1.11: icmp_seq=3 ttl=64 time=1.03 ms
    64 bytes from 10.101.1.11: icmp_seq=4 ttl=64 time=0.210 ms

    --- 10.101.1.11 ping statistics ---
    4 packets transmitted, 4 received, 0% packet loss, time 3020ms
    rtt min/avg/max/mdev = 0.210/0.636/1.027/0.378 ms
    ```
- Utiliser 1.1.1.1 comme serveur DNS
  - Sur la machine `node1.tp1.b2` :
    ```bash
    # Changement de serveur DNS
    [luka@node1 ~]$ sudo nano /etc/resolv.conf
    [luka@node1 ~]$ sudo cat /etc/resolv.conf
    nameserver 1.1.1.1

    # Vérification du bon fonctionnement
    [luka@node1 ~]$ dig ynov.com
    [...]
    ;; ANSWER SECTION:
    ynov.com.               300     IN      A       104.26.10.233
    ynov.com.               300     IN      A       172.67.74.226
    ynov.com.               300     IN      A       104.26.11.233

    ;; Query time: 39 msec
    ;; SERVER: 1.1.1.1#53(1.1.1.1)
    [...]
    ```
  - Sur la machine `node2.tp1.b2` :
    ```bash
    # Changement de serveur DNS
    [luka@node2 ~]$ sudo nano /etc/resolv.conf
    [luka@node2 ~]$ sudo cat /etc/resolv.conf
    nameserver 1.1.1.1

    # Vérification du bon fonctionnement
    [luka@node2 ~]$ dig ynov.com
    [...]
    ;; ANSWER SECTION:
    ynov.com.               275     IN      A       104.26.10.233
    ynov.com.               275     IN      A       172.67.74.226
    ynov.com.               275     IN      A       104.26.11.233

    ;; Query time: 29 msec
    ;; SERVER: 1.1.1.1#53(1.1.1.1)
    [...]
    ```
- Les machines doivent pouvoir se joindre par leurs noms respectifs
  - Ajout des noms dans le fichier `/etc/hosts`
    - Sur la machine `node1.tp1.b2` :
      ```bash
      [luka@node1 ~]$ sudo nano /etc/hosts
      [luka@node1 ~]$ cat /etc/hosts
      [...]
      10.101.1.12 node2 node2.tp1.b2
      ```
    - Sur la machine `node2.tp1.b2` :
      ```bash
      [luka@node2 ~]$ sudo nano /etc/hosts
      [luka@node2 ~]$ cat /etc/hosts
      [...]
      10.101.1.11 node1 node1.tp1.b2
      ```
  - Test de ping entre les deux machines
    - Sur la machine `node1.tp1.b2` :
      ```bash
      [luka@node1 ~]$ ping -c 4 node2
      PING node2 (10.101.1.12) 56(84) bytes of data.
      64 bytes from node2 (10.101.1.12): icmp_seq=1 ttl=64 time=0.635 ms
      64 bytes from node2 (10.101.1.12): icmp_seq=2 ttl=64 time=0.219 ms
      64 bytes from node2 (10.101.1.12): icmp_seq=3 ttl=64 time=0.303 ms
      64 bytes from node2 (10.101.1.12): icmp_seq=4 ttl=64 time=0.912 ms

      --- node2 ping statistics ---
      4 packets transmitted, 4 received, 0% packet loss, time 3118ms
      rtt min/avg/max/mdev = 0.219/0.517/0.912/0.275 ms
      ```
    - Sur la machine `node2.tp1.b2` :
      ```bash
      [luka@node2 ~]$ ping -c 4 node1
      PING node1 (10.101.1.11) 56(84) bytes of data.
      64 bytes from node1 (10.101.1.11): icmp_seq=1 ttl=64 time=0.495 ms
      64 bytes from node1 (10.101.1.11): icmp_seq=2 ttl=64 time=0.691 ms
      64 bytes from node1 (10.101.1.11): icmp_seq=3 ttl=64 time=1.04 ms
      64 bytes from node1 (10.101.1.11): icmp_seq=4 ttl=64 time=0.835 ms

      --- node1 ping statistics ---
      4 packets transmitted, 4 received, 0% packet loss, time 3037ms
      rtt min/avg/max/mdev = 0.495/0.764/1.036/0.197 ms
      ```
- Le pare-feu est configuré pour bloquer toutes les connexions exceptées celles qui sont nécessaires
  - Sur la machine `node1.tp1.b2` :
    ```bash
    [luka@node1 ~]$ sudo firewall-cmd --list-all
    public (active)
      target: default
      icmp-block-inversion: no
      interfaces: enp0s3 enp0s8
      sources:
      services: cockpit dhcpv6-client ssh
      ports:
      protocols:
      forward: yes
      masquerade: no
      forward-ports:
      source-ports:
      icmp-blocks:
      rich rules:
    ```
  - Sur la machine `node2.tp1.b2` :
    ```bash
    [luka@node2 ~]$ sudo firewall-cmd --list-all
    public (active)
      target: default
      icmp-block-inversion: no
      interfaces: enp0s3 enp0s8
      sources:
      services: cockpit dhcpv6-client ssh
      ports:
      protocols:
      forward: yes
      masquerade: no
      forward-ports:
      source-ports:
      icmp-blocks:
      rich rules:
    ```

## I. Utilisateurs

### 1. Création et configuration

🌞 **Ajouter un utilisateur à la machine, qui sera dédié à son administration**

- Sur la machine `node1.tp1.b2` :
  ```bash
  # Création de l'utilisateur
  [luka@node1 ~]$ sudo useradd toto -m -s /bin/bash

  # Vérification que l'utilisateur existe
  [luka@node1 ~]$ sudo cat /etc/passwd | grep "toto"
  toto:x:1001:1001::/home/toto:/bin/bash
  ```
- Sur la machine `node2.tp1.b2` :
  ```bash
  # Création de l'utilisateur
  [luka@node2 ~]$ sudo useradd toto -m -s /bin/bash

  # Vérification que l'utilisateur existe
  [luka@node2 ~]$ sudo cat /etc/passwd | grep "toto"
  toto:x:1001:1001::/home/toto:/bin/bash
  ```

🌞 **Créer un nouveau groupe admins** (avec les droits pour utiliser sudo)

- Sur la machine `node1.tp1.b2` :
  ```bash
  # Création du groupe
  [luka@node1 ~]$ sudo groupadd admins

  # Ajout des permissions au groupe pour utiliser sudo
  [luka@node1 ~]$ sudo visudo /etc/sudoers
  [luka@node1 ~]$ sudo cat /etc/sudoers | grep %admins
  %admins ALL=(ALL)       ALL
  ```
- Sur la machine `node2.tp1.b2` :
  ```bash
  # Création du groupe
  [luka@node2 ~]$ sudo groupadd admins
  
  # Ajout des permissions au groupe pour utiliser sudo
  [luka@node2 ~]$ sudo visudo /etc/sudoers
  [luka@node2 ~]$ sudo cat /etc/sudoers | grep %admins
  %admins ALL=(ALL)       ALL
  ```

🌞 **Ajouter votre utilisateur à ce groupe admins**

- Sur la machine `node1.tp1.b2` :
  ```bash
  # Ajout de l'utilisateur au groupe
  [luka@node2 ~]$ sudo usermod -aG admins toto
  [luka@node2 ~]$ groups toto
  toto : toto admins

  # Essai d'une commande avec sudo
  [luka@node1 ~]$ sudo su - toto
  [toto@node1 ~]$ sudo echo "Hello World!"
  Hello World!

  # Affichage des droits sudo de toto
  [toto@node1 ~]$ sudo -l
  [...]
  User toto may run the following commands on node1:
      (ALL) ALL
  ```
- Sur la machine `node2.tp1.b2` :
  ```bash
  # Ajout de l'utilisateur au groupe
  [luka@node2 ~]$ sudo usermod -aG admins toto
  [luka@node2 ~]$ groups toto
  toto : toto admins
  
  # Essai d'une commande avec sudo
  [luka@node2 ~]$ sudo su - toto
  [toto@node2 ~]$ sudo echo "Hello World!"
  Hello World!

  # Affichage des droits sudo de toto
  [toto@node2 ~]$ sudo -l
  [...]
  User toto may run the following commands on node1:
      (ALL) ALL
  ```

### 2. SSH

🌞 **Configuration d'un échange de clés SSH**

```bash
# Génération de la clé SSH
PS C:\Users\lukab> ssh-keygen -t rsa -b 4096
Enter file in which to save the key (C:\Users\lukab/.ssh/id_rsa):
Enter passphrase (empty for no passphrase):
Enter same passphrase again:
Your identification has been saved in C:\Users\lukab/.ssh/id_rsa.
Your public key has been saved in C:\Users\lukab/.ssh/id_rsa.pub.
The key fingerprint is:
SHA256:haez4bimX8zV90w7L9AkwarIEcHkd4ZOG4Zm4lbxKoQ lukab@PC_de_Luka
The key's randomart image is:
+---[RSA 3072]----+
|     o+.   .     |
|   . .o+ o  o    |
|  E o *.O =. .   |
|   o =.* O... .  |
|    +..oS.. .+. .|
|   . .o=.=  ...+.|
|      . *    . oo|
|      .o      . o|
|    .+o        ..|
+----[SHA256]-----+'

# On récupère la clé publique
PS C:\Users\lukab> cat .\.ssh\id_rsa.pub
ssh-rsa [...]

# Maintenant, sur la VM, on crée le fichier authorized_keys dans /home/toto/.ssh et on met la clé publique dedans (same for node2)
[toto@node1 ~]$ mkdir .ssh
[toto@node1 ~]$ nano .ssh/authorized_keys
[toto@node1 ~]$ cat .ssh/authorized_keys
ssh-rsa [...]

# On applique les bonne permissions (same for node2)
[toto@node1 .ssh]$ chmod 700 .
[toto@node1 .ssh]$ chmod 600 authorized_keys
[toto@node1 .ssh]$ ls -al
total 4
drwx------. 2 toto toto  29 Nov 14 15:18 .
drwx------. 3 toto toto  90 Nov 14 19:41 ..
-rw-------. 1 toto toto 570 Nov 14 20:11 authorized_keys
```

🌞 **Assurez vous que la connexion SSH est fonctionnelle**

- Connexion SSH à la machine `node1.tp1.b2` :
  ```
  PS C:\Users\lukab> ssh toto@10.101.1.11
  Last login: Mon Nov 14 20:18:06 2022 from 10.101.1.1
  [toto@node1 ~]$
  ```
- Connexion SSH à la machine `node2.tp1.b2` :
  ```
  PS C:\Users\lukab> ssh toto@10.101.1.12
  Last login: Mon Nov 14 20:03:19 2022
  [toto@node2 ~]$
  ```

## II. Partitionnement

### 2. Partitionnement

🌞 **Utilisez LVM**

- Affichage des disques à ajouter
  ```bash
  [toto@node1 ~]$ lsblk
  NAME        MAJ:MIN RM  SIZE RO TYPE MOUNTPOINTS
  [...]
  sdb           8:16   0    3G  0 disk
  sdc           8:32   0    3G  0 disk
  [...]
  ```
- Ajout des disques en tant que PV (Physical Volume)
  ```bash
  # Création des PV
  [toto@node1 ~]$ sudo pvcreate /dev/sdb
    Physical volume "/dev/sdb" successfully created.
  [toto@node1 ~]$ sudo pvcreate /dev/sdc
    Physical volume "/dev/sdc" successfully created.

  # Vérifications
  [toto@node1 ~]$ sudo pvs
    PV         VG Fmt  Attr PSize PFree
    /dev/sdb      lvm2 ---  3.00g 3.00g
    /dev/sdc      lvm2 ---  3.00g 3.00g

  [toto@node1 ~]$ sudo pvdisplay
    "/dev/sdb" is a new physical volume of "3.00 GiB"
    --- NEW Physical volume ---
    PV Name               /dev/sdb
    PV Size               3.00 GiB
    [...]

    "/dev/sdc" is a new physical volume of "3.00 GiB"
    --- NEW Physical volume ---
    PV Name               /dev/sdc
    PV Size               3.00 GiB
    [...]
  ```
- Agrégation des deux disques en un seul VG (Volume Group)
  ```bash
  # Création du VG avec sdb comme premier disque
  [toto@node1 ~]$ sudo vgcreate data /dev/sdb
    Volume group "data" successfully created

  # Ajout du disque sdc au VG
  [toto@node1 ~]$ sudo vgextend data /dev/sdc
    Volume group "data" successfully extended

  # Vérifications
  [toto@node1 ~]$ sudo vgs
    VG   #PV #LV #SN Attr   VSize VFree
    data   2   0   0 wz--n- 5.99g 5.99g

  [toto@node1 ~]$ sudo vgdisplay
    --- Volume group ---
    VG Name               data
    [...]
    VG Size               5.99 GiB
    PE Size               4.00 MiB
    [...]
  ```
- Création de 3 logical volumes de 1 Go chacun
  ```bash
  # Création des LV
  [toto@node1 ~]$ sudo lvcreate -L 1G data -n part1
    Logical volume "part1" created.
  [toto@node1 ~]$ sudo lvcreate -L 1G data -n part2
    Logical volume "part2" created.
  [toto@node1 ~]$ sudo lvcreate -L 1G data -n part3
    Logical volume "part3" created.

  # Vérifications
  [toto@node1 ~]$ sudo lvs
    LV    VG   Attr       LSize Pool Origin Data%  Meta%  Move Log Cpy%Sync Convert
    part1 data -wi-a----- 1.00g
    part2 data -wi-a----- 1.00g
    part3 data -wi-a----- 1.00g

  [toto@node1 ~]$ sudo lvdisplay
    --- Logical volume ---
    LV Path                /dev/data/part1
    LV Name                part1
    VG Name                data
    [...]
    LV Size                1.00 GiB
    [...]

    --- Logical volume ---
    LV Path                /dev/data/part2
    LV Name                part2
    VG Name                data
    [...]
    LV Size                1.00 GiB
    [...]

    --- Logical volume ---
    LV Path                /dev/data/part3
    LV Name                part3
    VG Name                data
    [...]
    LV Size                1.00 GiB
    [...]
  ```
- Formatage de ces partitions en ext4
  ```bash
    [toto@node1 ~]$ sudo mkfs -t ext4 /dev/data/part1
      mke2fs 1.46.5 (30-Dec-2021)
      Creating filesystem with 262144 4k blocks and 65536 inodes
      Filesystem UUID: d64ddc9a-34d2-4386-9b7c-4fece3c3fdd6
      Superblock backups stored on blocks:
              32768, 98304, 163840, 229376

      Allocating group tables: done
      Writing inode tables: done
      Creating journal (8192 blocks): done
      Writing superblocks and filesystem accounting information: done

    [toto@node1 ~]$ sudo mkfs -t ext4 /dev/data/part2
      mke2fs 1.46.5 (30-Dec-2021)
      Creating filesystem with 262144 4k blocks and 65536 inodes
      Filesystem UUID: 0ff8d684-eedf-47a2-897c-7dfa0d804273
      Superblock backups stored on blocks:
              32768, 98304, 163840, 229376

      Allocating group tables: done
      Writing inode tables: done
      Creating journal (8192 blocks): done
      Writing superblocks and filesystem accounting information: done

    [toto@node1 ~]$ sudo mkfs -t ext4 /dev/data/part3
      mke2fs 1.46.5 (30-Dec-2021)
      Creating filesystem with 262144 4k blocks and 65536 inodes
      Filesystem UUID: 3cba9538-6f50-44a4-bc66-3132094453bc
      Superblock backups stored on blocks:
              32768, 98304, 163840, 229376

      Allocating group tables: done
      Writing inode tables: done
      Creating journal (8192 blocks): done
      Writing superblocks and filesystem accounting information: done
  ```
- Montage de ces partitions
  ```bash
  # Création des dossiers pour monter la partition
  [toto@node1 ~]$ sudo mkdir /mnt/part1 && sudo mkdir /mnt/part2 && sudo mkdir /mnt/part3
  [toto@node1 ~]$ ls /mnt
  part1  part2  part3

  # Montage des partitions
  [toto@node1 ~]$ sudo mount /dev/data/part1 /mnt/part1
  [toto@node1 ~]$ sudo mount /dev/data/part2 /mnt/part2
  [toto@node1 ~]$ sudo mount /dev/data/part3 /mnt/part3

  # Vérifications
  [toto@node1 ~]$ df -h
    Filesystem              Size  Used Avail Use% Mounted on
    [...]
    /dev/mapper/data-part1  974M   24K  907M   1% /mnt/part1
    /dev/mapper/data-part2  974M   24K  907M   1% /mnt/part2
    /dev/mapper/data-part3  974M   24K  907M   1% /mnt/part3
  ```

🌞 **Montage automatique de la partition au démarrage du système**

```bash
  # Définition du montage automatique
  [toto@node1 ~]$ sudo nano /etc/fstab
  [toto@node1 ~]$ cat /etc/fstab | grep "part"
  /dev/data/part1 /mnt/part1      ext4    defaults        0 0
  /dev/data/part2 /mnt/part2      ext4    defaults        0 0
  /dev/data/part3 /mnt/part3      ext4    defaults        0 0

  # Vérification
  [toto@node1 ~]$ sudo umount /mnt/part1
  [toto@node1 ~]$ sudo umount /mnt/part2
  [toto@node1 ~]$ sudo umount /mnt/part3
  [toto@node1 ~]$ sudo mount -av
    [...]
    /mnt/part1               : successfully mounted
    /mnt/part2               : successfully mounted
    /mnt/part3               : successfully mounted
```

## III. Gestion de services

### 1. Interaction avec un service existant

🌞 **Petits tests sur un service existant**

- Assurez vous que l'unité est démarrée
  ```bash
  [toto@node1 ~]$ systemctl is-active firewalld
  active
  ```
- Assurez vous que l'unitée est activée (elle se lance automatiquement au démarrage)
  ```bash
  [toto@node1 ~]$ systemctl is-enabled firewalld
  enabled
  ```

### 2. Création de service

### A. Unité simpliste

🌞 **Créer un fichier qui définit une unité de service**

```bash
  # Création du fichier définissant une unité de service
  [toto@node1 ~]$ sudo nano /etc/systemd/system/web.service
  [toto@node1 ~]$ cat /etc/systemd/system/web.service
  [Unit]
  Description=Very simple web service

  [Service]
  ExecStart=/usr/bin/python3 -m http.server 8888

  [Install]
  WantedBy=multi-user.target

  # Rechargement des fichiers de configuration
  [toto@node1 ~]$ sudo systemctl daemon-reload

  # Ouverture du port 8888 sur la machine
  [toto@node1 ~]$ sudo firewall-cmd --add-port=8888/tcp --permanent
  success
  [toto@node1 ~]$ sudo firewall-cmd --reload
  success

  # Intéractions avec le service
  [toto@node1 ~]$ sudo systemctl status web
  ○ web.service - Very simple web service
      Loaded: loaded (/etc/systemd/system/web.service; disabled; vendor preset: disabled)
      Active: inactive (dead)
  [toto@node1 ~]$ sudo systemctl start web
  [toto@node1 ~]$ sudo systemctl enable web
  Created symlink /etc/systemd/system/multi-user.target.wants/web.service → /etc/systemd/system/web.service.
```

🌞 **Une fois le service démarré, assurez-vous que pouvez accéder au serveur web**

```bash
  [toto@node2 ~]$ curl 10.101.1.11:8888
  <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
  <html>
    [...]
  </html>
```

### B. Modification de l'unité

🌞 **Préparez l'environnement pour exécuter le mini serveur web Python**

- Création d'un utilisateur `web`
  ```bash
  # Création de l'utilisateur
  [toto@node1 ~]$ sudo useradd web

  # Vérification
  [toto@node1 ~]$ sudo cat /etc/passwd | grep "web"
  web:x:1002:1003::/home/web:/bin/bash
  ```
- Création d'un dossier `/var/www/meow`
  ```bash
  # Création du dossier
  [toto@node1 ~]$ sudo mkdir /var/www
  [toto@node1 ~]$ sudo mkdir /var/www/meow

  # Don de la propriété du dossier à l'utilisateur web
  [toto@node1 ~]$ sudo chown web:web /var/www/meow

  # Verification
  [toto@node1 ~]$ ls -al /var/www | grep "meow"
  drwxr-xr-x.  2 web  web     6 Nov 14 19:56 meow
  ```
- Création d'un fichier dans `/var/www/meow`
  ```bash
  # Passage en utilisateur web
  [toto@node1 ~]$ sudo su - web

  # Création du fichier
  [web@node1 ~]$ echo "MEOW" | touch /var/www/meow/meow.html
  ```
- Affichage des permissions du dossier `/var/www/meow` et son contenu
  ```bash
  [web@node1 ~]$ ls -al /var/www/meow
  total 0
  drwxr-xr-x. 2 web  web  23 Nov 14 20:00 .           # Permissions du dossier
  drwxr-xr-x. 3 root root 18 Nov 14 19:56 ..
  -rw-r--r--. 1 web  web   0 Nov 14 20:00 meow.html   # Permissions du fichier
  ```

🌞 **Modification de l'unité de service web.service**

```bash
  # Écriture des changements
  [toto@node1 ~]$ sudo nano /etc/systemd/system/web.service

  # Vérification
  [toto@node1 ~]$ cat /etc/systemd/system/web.service
  [Unit]
  Description=Very simple web service

  [Service]
  ExecStart=/usr/bin/python3 -m http.server 8888
  User=web
  WorkingDirectory=/var/www/meow/

  [Install]
  WantedBy=multi-user.target

  # Rechargement de la conf des services
  [toto@node1 ~]$ sudo systemctl daemon-reload
  [toto@node1 ~]$ sudo systemctl restart web
```

🌞 **Vérification du bon fonctionnement avec une commande curl**

```bash
  [toto@node2 ~]$ curl 10.101.1.11:8888
  [...]
  <li><a href="meow.html">meow.html</a></li>
  [...]

  # On retrouve bien notre fichier meow.html, donc on est dans le bon directory !
```